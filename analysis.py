from parse import parse
import sys

f = open(sys.argv[1], 'r')

total_received = 0;
total_delay = 0;

while True:
	l = f.readline()
	if (l == ""):
		break

	if (l[0] == 'r'):
		total_received += int(parse('received_block_count: {}', l)[0])
		# print(parse('received_block_count: {}', l)[0]);

	if (l[0] == 't'):
		total_delay += int(parse('total_delay: {}', l)[0])
		# print(parse('total_delay: {}', l)[0]);

print(total_delay/total_received)