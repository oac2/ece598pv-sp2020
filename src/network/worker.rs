use crate::transaction::verify;

use crate::crypto::hash::H160;

use super::message::Message;
use std::sync::{Arc, Mutex};
use crate::blockchain::{Blockchain, Mempool, State, Account};

use crate::crypto::hash::{Hashable};
use crate::block::{Block};

use super::peer;
use crate::network::server::Handle as ServerHandle;
use crossbeam::channel;
use log::{debug, warn, info};

use std::thread;

use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    mempool: Arc<Mutex<Mempool>>,
    state: Arc<Mutex<State>>
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<Mempool>>,
    state: &Arc<Mutex<State>>
) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        blockchain: blockchain.clone(),
        mempool: mempool.clone(),
        state: state.clone()
    }
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&self) {
        let mut buffer: Vec<Block> = Vec::new();

        let mut received_block_count: u128 = 0;
        let mut total_delay: u128 = 0;

        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            match msg {
                Message::NewTransactionHashes(hashes) => {
                    log::info!("Received transaction: {:?}", hashes);
                    let mempool = self.mempool.lock().unwrap();
                    let transaction_table  = &mempool.table;
                    let mut not_contained = Vec::new();

                    for hash in hashes {
                        if !transaction_table.contains_key(&hash) {
                            not_contained.push(hash);

                            self.server.broadcast(Message::NewTransactionHashes(vec![hash]));
                        }
                    }

                    peer.write(Message::GetTransactions(not_contained));
                }

                Message::GetTransactions(transactions) => {
                    let mempool = self.mempool.lock().unwrap();
                    let transaction_table  = &mempool.table;
                    let mut retrieved_transactions = Vec::new();

                    let mut not_contained = Vec::new();

                    for hash in transactions {
                        if transaction_table.contains_key(&hash) {
                            retrieved_transactions.push(transaction_table[&hash].clone());
                            // debug!("Retrieved Block {}", hash);
                        } else {
                            not_contained.push(hash);
                        }
                    }
                    if not_contained.len() != 0 {
                        peer.write(Message::GetTransactions(not_contained));
                    }
                    peer.write(Message::Transactions(retrieved_transactions));
                }

                Message::Transactions(transactions) => {
                    let mut mempool = self.mempool.lock().unwrap();

                    for transaction in transactions {
                        if mempool.contains_key(&transaction.hash()) {
                            continue;
                        }

                        self.server.broadcast(Message::NewTransactionHashes(vec![transaction.hash()]));

                        mempool.insert_transaction(&transaction);
                    }
                }

                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }
                Message::NewBlockHashes(hashes) => {
                    let blockchain = self.blockchain.lock().unwrap();
                    let mut not_contained = Vec::new();
                    // debug!("Tip Blockchain {}", blockchain.tip());
                    for hash in hashes {
                        if !blockchain.chain.contains_key(&hash) {
                            not_contained.push(hash);
                            // See https://piazza.com/class/k5n0djonh5k2rs?cid=144
                            // If this is a unseen block, broadcast to everyone else on the network.
                            self.server.broadcast(Message::NewBlockHashes(vec![hash]));
                            // debug!("NewBlockHashes blocks not contained {}", hash);
                        }
                    }

                    // TODO: Fix gossip Node 1 -> Node 2 -> Node 3.
                    // https://piazza.com/class/k5n0djonh5k2rs?cid=120 (maybe?)
                    // self.server.broadcast(Message::NewBlockHashes(not_contained.clone()));

                    peer.write(Message::GetBlocks(not_contained));
                }
                Message::GetBlocks(blocks) => {
                    let blockchain = self.blockchain.lock().unwrap();
                    let mut retrieved_blocks = Vec::new();
                    // When C ask for a block that B doesn't have, B should forward the request to A.
                    let mut not_contained = Vec::new();

                    for hash in blocks {
                        if blockchain.chain.contains_key(&hash) {
                            retrieved_blocks.push(blockchain.chain[&hash].clone());
                            // debug!("Retrieved Block {}", hash);
                        } else {
                            not_contained.push(hash);
                        }
                    }
                    if not_contained.len() != 0 {
                        peer.write(Message::GetBlocks(not_contained));
                    }
                    peer.write(Message::Blocks(retrieved_blocks));
                }
                Message::Blocks(blocks) => {
                    let mut blockchain = match self.blockchain.lock() {
                        Ok(guard) => guard,
                        Err(poisoned) => poisoned.into_inner()
                    };

                    let mut state = self.state.lock().unwrap();

                    // let mut not_contained = Vec::new();
                    let received_timestamp = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();

                    for block in blocks {
                        // The block is already in the block chain.
                        if blockchain.chain.contains_key(&block.hash()) {
                            continue;
                        }

                        self.server.broadcast(Message::NewBlockHashes(vec![block.hash()]));

                        // not_contained.push(block.hash());
                        // The block is not in the block chain but its parent is in the chain.

                        // total_delay = total_delay + received_timestamp - block.header.timestamp.duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();
                        // received_block_count = received_block_count + 1;

                        if blockchain.chain.contains_key(&(block.header.parent)) && !blockchain.chain.contains_key(&block.hash()) {
                            if blockchain.chain[&block.header.parent].header.difficulty == block.header.difficulty && block.hash() <= block.header.difficulty {

                                let mut prev_state = state.block_state[&block.header.parent].clone();

                                let mut valid = true;

                                // Execute block transactions.
                                // Check validity of all transactions in the block before inserting to blockchain.
                                for transaction in &block.data {
                                    let temp_pubkey = &transaction.pubkey;
                                    let mut temp_sign :[u8; 64] = [0; 64];
                                    temp_sign.copy_from_slice(&[&transaction.sign_p1[..], &transaction.sign_p2[..]].concat());
                                    if !verify(&transaction, temp_pubkey, &temp_sign) {
                                        valid = false;
                                        break;
                                    }

                                    let sender_addr : H160 = ring::digest::digest(&ring::digest::SHA256, &transaction.pubkey).into();

                                    let receiver_addr = transaction.dest;

                                    if !prev_state.contains_key(&sender_addr) {
                                        valid = false;
                                        break;
                                    }

                                    let mut send_account = prev_state[&sender_addr].clone();
                                    send_account.balance -= transaction.input;
                                    send_account.nonce += 1;

                                    if send_account.balance < 0.0 || transaction.nonce != send_account.nonce {
                                        valid = false;
                                        break;
                                    }

                                    prev_state.insert(sender_addr, send_account);

                                    if !prev_state.contains_key(&receiver_addr) {
                                        prev_state.insert(receiver_addr, Account{nonce: 0, balance: 0.0});
                                    }

                                    let mut rec_account = prev_state[&receiver_addr].clone();
                                    rec_account.balance += transaction.input;

                                    prev_state.insert(receiver_addr, rec_account);

                                }

                                if valid {
                                    blockchain.insert(&block);
                                    state.block_state.insert(block.hash(), prev_state);
                                    log::info!("[State: {:?}]", state.block_state[&blockchain.tip()]);
                                    // Check mempool to remove invalid transactions

                                    total_delay = total_delay + received_timestamp - block.header.timestamp.duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();
                                    received_block_count = received_block_count + 1;
                                    log::info!("[{}]-[Tip hash {}] [Parent hash{}]", blockchain.height[&blockchain.tip()], blockchain.tip(), block.header.parent);

                                }
                                
                            }
                        // Neither the block nor its parent is in the block chain.
                        } else {
                            // debug!("Block not found {} {}", block.header.parent, block.hash());
                            buffer.push(block.clone());
                            // TODO: Create Buffer for missing parent.
                            // Place buffer in worker loop but not inside this match.
                            // Get its parent from other nodes.
                            peer.write(Message::GetBlocks(vec![block.header.parent]));
                        }
                    }

                    if received_block_count != 0 {
                        // println!("total_delay: {:?}", total_delay);
                        // println!("received_block_count: {:?}", received_block_count);
                        // println!("Average delay: {:?}", total_delay / received_block_count);
                    }
                    

                    // self.server.broadcast(Message::NewBlockHashes(not_contained));

                    let mut insert_from_buffer = true;
                    while insert_from_buffer {
                        for idx in 0..(buffer.len()) {
                            if blockchain.chain.contains_key(&buffer[idx].header.parent) {
                                let block = &buffer[idx].clone();

                                if blockchain.chain[&block.header.parent].header.difficulty == block.header.difficulty && block.hash() <= block.header.difficulty {

                                    let mut prev_state = state.block_state[&block.header.parent].clone();

                                    let mut valid = true;

                                    // Execute block transactions.
                                    // Check validity of all transactions in the block before inserting to blockchain.
                                    for transaction in &block.data {
                                        let temp_pubkey = &transaction.pubkey;
                                        let mut temp_sign :[u8; 64] = [0; 64];
                                        temp_sign.copy_from_slice(&[&transaction.sign_p1[..], &transaction.sign_p2[..]].concat());

                                        if !verify(&transaction, temp_pubkey, &temp_sign) {
                                            valid = false;
                                            break;
                                        }


                                        let sender_addr : H160 = ring::digest::digest(&ring::digest::SHA256, &transaction.pubkey).into();

                                        let receiver_addr = transaction.dest;

                                        if !prev_state.contains_key(&sender_addr) {
                                            valid = false;
                                            break;
                                        }

                                        let mut send_account = prev_state[&sender_addr].clone();
                                        send_account.balance -= transaction.input;
                                        send_account.nonce += 1;

                                        if prev_state[&sender_addr].balance < 0.0 || transaction.nonce != prev_state[&sender_addr].nonce {
                                            valid = false;
                                            break;
                                        }

                                        prev_state.insert(sender_addr, send_account);

                                        if !prev_state.contains_key(&receiver_addr) {
                                            prev_state.insert(receiver_addr, Account{nonce: 0, balance: 0.0});
                                        }

                                        let mut rec_account = prev_state[&receiver_addr].clone();
                                        rec_account.balance += transaction.input;

                                        prev_state.insert(receiver_addr, rec_account);

                                    }

                                    buffer.remove(idx);
                                    insert_from_buffer = false;

                                    if valid {
                                        blockchain.insert(&block);
                                        insert_from_buffer = true;
                                        state.block_state.insert(block.hash(), prev_state);
                                        log::info!("[State: {:?}]", state.block_state[&blockchain.tip()]);
                                        // Check mempool to remove invalid transactions

                                        // total_delay = total_delay + received_timestamp - block.header.timestamp.duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();
                                        // received_block_count = received_block_count + 1;
                                
                                        log::info!("[{}]-[Tip hash {}] [Parent hash {}]", blockchain.height[&blockchain.tip()], blockchain.tip(), block.header.parent);
                                        break;
                                    }            
                                }
                            } else {
                                insert_from_buffer = false;
                            }
                        }
                        if buffer.len() == 0 {
                            insert_from_buffer = false;
                        }
                    }
                }
            }
        }
    }
}
