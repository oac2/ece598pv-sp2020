use crate::crypto::hash::H160;

use crate::blockchain::Mempool;

use std::sync::Arc;
use std::sync::Mutex;
use crate::crypto::hash::{Hashable};

use crate::transaction::{Transaction, sign};
use serde::Serialize;
use crate::miner::Handle as MinerHandle;
use crate::network::server::Handle as NetworkServerHandle;
use crate::network::message::Message;

use log::info;
use std::collections::HashMap;
use std::thread;
use tiny_http::Header;
use tiny_http::Response;
use tiny_http::Server as HTTPServer;
use url::Url;


pub struct Server {
    handle: HTTPServer,
    miner: MinerHandle,
    network: NetworkServerHandle,
    mempool: Arc<Mutex<Mempool>>,
}

#[derive(Serialize)]
struct ApiResponse {
    success: bool,
    message: String,
}

macro_rules! respond_result {
    ( $req:expr, $success:expr, $message:expr ) => {{
        let content_type = "Content-Type: application/json".parse::<Header>().unwrap();
        let payload = ApiResponse {
            success: $success,
            message: $message.to_string(),
        };
        let resp = Response::from_string(serde_json::to_string_pretty(&payload).unwrap())
            .with_header(content_type);
        $req.respond(resp).unwrap();
    }};
}

impl Server {
    pub fn start(
        addr: std::net::SocketAddr,
        miner: &MinerHandle,
        network: &NetworkServerHandle,
        mempool: &Arc<Mutex<Mempool>>,
    ) {
        let handle = HTTPServer::http(&addr).unwrap();
        let server = Self {
            handle,
            miner: miner.clone(),
            network: network.clone(),
            mempool: mempool.clone()
        };
        thread::spawn(move || {
            for req in server.handle.incoming_requests() {
                let miner = server.miner.clone();
                let network = server.network.clone();
                let mempool = server.mempool.clone();
                thread::spawn(move || {
                    // a valid url requires a base
                    let base_url = Url::parse(&format!("http://{}/", &addr)).unwrap();
                    let url = match base_url.join(req.url()) {
                        Ok(u) => u,
                        Err(e) => {
                            respond_result!(req, false, format!("error parsing url: {}", e));
                            return;
                        }
                    };
                    match url.path() {
                        "/miner/start" => {
                            let params = url.query_pairs();
                            let params: HashMap<_, _> = params.into_owned().collect();
                            let lambda = match params.get("lambda") {
                                Some(v) => v,
                                None => {
                                    respond_result!(req, false, "missing lambda");
                                    return;
                                }
                            };
                            let lambda = match lambda.parse::<u64>() {
                                Ok(v) => v,
                                Err(e) => {
                                    respond_result!(
                                        req,
                                        false,
                                        format!("error parsing lambda: {}", e)
                                    );
                                    return;
                                }
                            };
                            miner.start(lambda);
                            respond_result!(req, true, "ok");
                        }
                        "/miner/exit" => {
                            miner.exit();
                            respond_result!(req, true, "ok");
                        }
                        "/network/ping" => {
                            network.broadcast(Message::Ping(String::from("Test ping")));
                            respond_result!(req, true, "ok");
                        }
                        "/transaction" => {
                            let params = url.query_pairs();
                            let params: HashMap<_, _> = params.into_owned().collect();
                            let src = match params.get("src") {
                                Some(v) => v,
                                None => {
                                    respond_result!(req, false, "missing src");
                                    return;
                                }
                            };
                            let dest = match params.get("dest") {
                                Some(v) => v,
                                None => {
                                    respond_result!(req, false, "missing dest");
                                    return;
                                }
                            };

                            let nonce = match params.get("nonce") {
                                Some(v) => v,
                                None => {
                                    respond_result!(req, false, "missing dest");
                                    return;
                                }
                            };
                            let nonce = match nonce.parse::<u32>() {
                                Ok(v) => v,
                                Err(e) => {
                                    respond_result!(
                                        req,
                                        false,
                                        format!("error parsing dest: {}", e)
                                    );
                                    return;
                                }
                            };

                            let input = match params.get("input") {
                                Some(v) => v,
                                None => {
                                    respond_result!(req, false, "missing dest");
                                    return;
                                }
                            };
                            let input = match input.parse::<f32>() {
                                Ok(v) => v,
                                Err(e) => {
                                    respond_result!(
                                        req,
                                        false,
                                        format!("error parsing dest: {}", e)
                                    );
                                    return;
                                }
                            };
                            let mut pubkey: [u8; 32] = [0; 32];
                            pubkey[..].copy_from_slice(&hex::decode(src).unwrap());

                            let mut destination: [u8; 20] = [0; 20];
                            destination[..].copy_from_slice(&hex::decode(dest).unwrap());

                            let blah : H160 = ring::digest::digest(&ring::digest::SHA256, &pubkey).into();
                            let be : [u8; 20] = blah.into();

                            println!("{:?} {:?}", dest, hex::encode(be));

                            let mut t = Transaction{nonce: nonce, dest:destination.into(), pubkey:pubkey, sign_p1:[0;32], sign_p2:[0;32], input:input};

                            let arr = [
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 13, 208, 250, 148, 21, 19, 229, 37, 245, 79, 199, 247, 181, 120, 85, 160, 110, 36, 14, 245, 130, 47, 22, 193, 81, 79, 217, 20, 136, 3, 220, 253, 161, 35, 3, 33, 0, 173, 95, 67, 218, 192, 253, 196, 211, 31, 165, 115, 108, 5, 86, 8, 95, 171, 241, 128, 171, 171, 81, 145, 11, 165, 100, 215, 3, 52, 113, 78, 45],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 83, 95, 217, 16, 124, 64, 58, 29, 68, 169, 26, 2, 116, 0, 38, 94, 100, 119, 185, 152, 136, 237, 209, 131, 35, 187, 213, 85, 9, 24, 156, 227, 161, 35, 3, 33, 0, 152, 139, 106, 190, 33, 2, 89, 15, 238, 250, 90, 170, 177, 194, 35, 194, 208, 69, 98, 153, 161, 246, 143, 74, 129, 204, 28, 252, 62, 56, 201, 192],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 52, 249, 50, 10, 226, 195, 95, 208, 177, 35, 71, 163, 29, 186, 149, 198, 163, 207, 143, 74, 235, 16, 77, 93, 73, 32, 249, 248, 243, 84, 237, 238, 161, 35, 3, 33, 0, 184, 184, 70, 141, 125, 86, 19, 226, 85, 116, 15, 66, 17, 119, 90, 253, 232, 215, 44, 201, 220, 248, 18, 193, 84, 90, 207, 34, 41, 141, 121, 95],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 201, 33, 78, 125, 84, 43, 44, 80, 53, 90, 222, 254, 20, 176, 82, 247, 162, 10, 19, 138, 20, 111, 49, 227, 107, 213, 183, 69, 196, 150, 16, 70, 161, 35, 3, 33, 0, 74, 71, 242, 92, 157, 163, 242, 181, 251, 129, 211, 184, 167, 108, 208, 175, 211, 240, 218, 42, 192, 51, 13, 209, 249, 174, 253, 59, 14, 50, 12, 31],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 102, 220, 146, 76, 65, 207, 93, 103, 201, 77, 165, 96, 128, 178, 228, 223, 188, 203, 17, 201, 34, 237, 96, 211, 85, 136, 253, 33, 187, 95, 36, 19, 161, 35, 3, 33, 0, 249, 53, 168, 70, 99, 176, 2, 166, 110, 132, 120, 146, 11, 72, 116, 214, 79, 6, 102, 18, 177, 160, 205, 156, 69, 245, 116, 194, 172, 136, 200, 42],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 99, 126, 69, 139, 73, 204, 23, 77, 16, 72, 141, 181, 93, 72, 85, 103, 164, 246, 234, 38, 49, 116, 77, 212, 241, 124, 11, 19, 249, 116, 236, 223, 161, 35, 3, 33, 0, 30, 143, 199, 239, 69, 90, 157, 238, 234, 195, 249, 73, 0, 45, 9, 115, 224, 13, 154, 70, 177, 162, 167, 150, 183, 62, 8, 208, 251, 26, 139, 6],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 160, 220, 62, 70, 83, 253, 104, 250, 5, 143, 75, 98, 106, 13, 95, 250, 113, 173, 148, 33, 60, 162, 61, 173, 98, 236, 215, 93, 214, 37, 155, 127, 161, 35, 3, 33, 0, 118, 195, 88, 234, 196, 32, 73, 99, 73, 167, 70, 153, 191, 219, 105, 14, 34, 212, 5, 27, 117, 39, 191, 46, 130, 175, 40, 104, 7, 81, 42, 26],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 113, 102, 222, 157, 249, 45, 104, 211, 209, 70, 176, 42, 162, 211, 119, 232, 41, 94, 194, 155, 61, 143, 170, 197, 101, 179, 15, 116, 75, 154, 248, 75, 161, 35, 3, 33, 0, 154, 21, 195, 35, 138, 123, 166, 189, 8, 174, 28, 138, 119, 119, 20, 225, 80, 62, 42, 249, 51, 16, 198, 255, 218, 77, 99, 171, 140, 82, 126, 0],
                                vec![48, 83, 2, 1, 1, 48, 5, 6, 3, 43, 101, 112, 4, 34, 4, 32, 191, 40, 109, 41, 153, 93, 62, 124, 207, 246, 210, 153, 171, 51, 226, 243, 115, 22, 94, 205, 198, 17, 61, 118, 86, 140, 239, 67, 155, 228, 238, 79, 161, 35, 3, 33, 0, 22, 41, 4, 204, 76, 54, 14, 132, 56, 85, 208, 71, 74, 25, 205, 101, 76, 27, 60, 41, 162, 248, 218, 96, 32, 34, 202, 227, 33, 22, 208, 87]
                            ];
                            let pks = [
                                "ad5f43dac0fdc4d31fa5736c0556085fabf180abab51910ba564d70334714e2d",
                                "988b6abe2102590feefa5aaab1c223c2d0456299a1f68f4a81cc1cfc3e38c9c0",
                                "b8b8468d7d5613e255740f4211775afde8d72cc9dcf812c1545acf22298d795f",
                                "4a47f25c9da3f2b5fb81d3b8a76cd0afd3f0da2ac0330dd1f9aefd3b0e320c1f",
                                "f935a84663b002a66e8478920b4874d64f066612b1a0cd9c45f574c2ac88c82a",
                                "1e8fc7ef455a9deeeac3f949002d0973e00d9a46b1a2a796b73e08d0fb1a8b06",
                                "76c358eac420496349a74699bfdb690e22d4051b7527bf2e82af286807512a1a",
                                "9a15c3238a7ba6bd08ae1c8a777714e1503e2af93310c6ffda4d63ab8c527e00",
                                "162904cc4c360e843855d0474a19cd654c1b3c29a2f8da602022cae32116d057"
                            ];
                            let mut key;
                            use ring::signature::Ed25519KeyPair;
                            let mut m = mempool.lock().unwrap();
                            for val in 0..9 {
                                key = Ed25519KeyPair::from_pkcs8(&arr[val]).unwrap();//key_pair::same(&val);
                                if pks[val] == src {
                                    let sign = sign(&t, &key);
                                    let mut sign_p1: [u8; 32] = [0;32];
                                    sign_p1.copy_from_slice(&sign.as_ref()[..32]);
                                    let mut sign_p2: [u8; 32] = [0; 32];
                                    sign_p2.copy_from_slice(&sign.as_ref()[32..]);
                                    t.sign_p1 = sign_p1;
                                    t.sign_p2 = sign_p2;

                                    // Add transaction to mempool.
                                    network.broadcast(Message::NewTransactionHashes(vec![t.hash()]));
                                    m.insert_transaction(&t);
                                    info!("Created transaction {}", t.hash());
                                    break;
                                }
                            } 
                            respond_result!(req, true, "ok");
                            
                        }
                        _ => {
                            let content_type =
                                "Content-Type: application/json".parse::<Header>().unwrap();
                            let payload = ApiResponse {
                                success: false,
                                message: "endpoint not found".to_string(),
                            };
                            let resp = Response::from_string(
                                serde_json::to_string_pretty(&payload).unwrap(),
                            )
                            .with_header(content_type)
                            .with_status_code(404);
                            req.respond(resp).unwrap();
                        }
                    }
                });
            }
        });
        info!("API server listening at {}", &addr);
    }
}
