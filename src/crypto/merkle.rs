use super::hash::{Hashable, H256};

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    data: Vec<Vec<H256>>
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let mut dat: Vec<H256> = Vec::new();
        let mut everything: Vec<Vec<H256>> = Vec::new();
        for num in data {
            dat.push(num.hash())
        }
        if dat.len() % 2 != 0 {
            dat.push(data.last().unwrap().hash())
        }
        everything.push(dat);

        while everything.last().unwrap().len() > 1 {
            let last = everything.last().unwrap();
            dat = Vec::new();
            for num in 0..(last.len()/2) {
                let mut ctx = ring::digest::Context::new(&ring::digest::SHA256);
                ctx.update(last[num*2].as_ref());
                ctx.update(last[num*2 + 1].as_ref());
                let fi: H256 = ctx.finish().into();
                dat.push(fi);
            }
            if dat.len() % 2 != 0 && dat.len() != 1 {
                dat.push(dat.last().cloned().unwrap());
            }
            everything.push(dat);

        }
        MerkleTree{data: everything}
    }

    pub fn root(&self) -> H256 {
        if self.data.last().unwrap().len() == 0 {
            return (hex!("0000000000000000000000000000000000000000000000000000000000000000")).into();
        } else {
            return self.data.last().unwrap()[0];
        }
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut p = Vec::new();
        let mut curr_index : usize = index;
        for level in 0..(self.data.len()-1)  {
            if curr_index % 2 == 0 {
                println!("{:?}", self.data[level][curr_index+1]);
                p.push(self.data[level][curr_index+1]);
                curr_index = curr_index / 2;
            } else {
                println!("{:?}", self.data[level][curr_index-1]);
                p.push(self.data[level][curr_index-1]);
                curr_index = (curr_index-1)/2;
            }
        }
        return p;
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, _leaf_size: usize) -> bool {
    let mut curr_index = index;
    let mut node : H256 = *datum;
    for num in proof {
        let mut ctx = ring::digest::Context::new(&ring::digest::SHA256);
        if curr_index % 2 == 0 {
            ctx.update(node.as_ref());
            ctx.update(num.as_ref());
            node = ctx.finish().into();
            curr_index = curr_index / 2;
        } else {
            ctx.update(num.as_ref());
            ctx.update(node.as_ref());
            node = ctx.finish().into();
            curr_index = (curr_index-1)/2;
        }
    }
    return *root == node;
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),


                // (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0a")).into(),
                // (hex!("0101010101010101010101010101010101010101010101010101010101010203")).into(),
                // (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0b")).into(),
                // (hex!("0101010101010101010101010101010101010101010101010101010101010201")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()


            // (hex!("a36d3a285ba91b468c919268efac8bd1dad1a4ef04244ed971dfec3694f3cd89")).into()
            //  cc171ecb295653211a2fb0321d942f5e481b12900ccd9513bc8b43ddfa54b66c 38622f21cf4c5da7431deda521e238e7d76ffba25b19bb057dc84c4006a3f8f4
            //  428b47dc13767d268c1f52b17572ec9e4a88a8cc55dec01595c55ab8ecf3d4e1 4f86a025fab5f347e232d2d32794264405396d265cf75e0de98f3617587f84ed

            // 6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920 fe63f9c6dab1122e59b87d185bdc57312c2478ff28669f53e042fa5ee0545361
            // 0107297842e617e0348bfb073641b89fffbda5b16a85a6fcc7d8d3b457332d61 0107297842e617e0348bfb073641b89fffbda5b16a85a6fcc7d8d3b457332d61

            // 4ee61ce80702218f7c7328aa63cc8a5700b9cd22de132961e7e86a01b675ec7d 531abc924c7b3566d0fef7cb3945b02c76cf7a3b5ec604f4b5c66fbb276c924d

            // a36d3a285ba91b468c919268efac8bd1dad1a4ef04244ed971dfec3694f3cd89

        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![
                   hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into(),


                   // hex!("fe63f9c6dab1122e59b87d185bdc57312c2478ff28669f53e042fa5ee0545361").into(),
                   // hex!("531abc924c7b3566d0fef7cb3945b02c76cf7a3b5ec604f4b5c66fbb276c924d").into()
                   ]

                   // vec![
                   //  hex!("38622f21cf4c5da7431deda521e238e7d76ffba25b19bb057dc84c4006a3f8f4").into(),


                   // hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920").into(),
                   // hex!("531abc924c7b3566d0fef7cb3945b02c76cf7a3b5ec604f4b5c66fbb276c924d").into()
                   // ]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        for index in 0..input_data.len() {
            let proof = merkle_tree.proof(index);
            assert!(verify(&merkle_tree.root(), &input_data[index].hash(), &proof, index, input_data.len()));
        }
        
    }
}
