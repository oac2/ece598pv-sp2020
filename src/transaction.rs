use crate::crypto::hash::H160;
use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use rand::Rng;

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy)]
pub struct Transaction {
    pub nonce: u32,
    pub input: f32,
    pub pubkey: [u8; 32],
    pub sign_p1: [u8; 32],
    pub sign_p2: [u8; 32],
    pub dest: H160
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let ser = bincode::serialize(&t).unwrap();
    let sign = key.sign(&ser);

    return sign;
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &[u8], signature: &[u8]) -> bool {
    let mut temp_trans = t.clone(); 
    temp_trans.sign_p1 = [0; 32];
    temp_trans.sign_p2 = [0; 32];

    let ser = bincode::serialize(&temp_trans).unwrap();
    // EdDSAParameters.verify(public_key.into(), ser.into(), signature.into());
    let pubkey = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, public_key);
    let ver = pubkey.verify(&ser, signature);
    match ver {
        Ok(_v) => return true,
        Err(_e) => return false,

    }
    // let unsign = public_key
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        let mut rng = rand::thread_rng();
        let dest : H160 = (hex!("0000000000000000000000000000000000000000")).into();
        let t = Transaction{nonce: 0, dest:dest, pubkey:[0; 32], sign_p1:[0; 32], sign_p2:[0;32], input:rng.gen::<f32>()};
        return t;
    }

    #[test]
    fn sign_verify() {
        let mut t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);

        // t = generate_random_transaction();
        assert!(verify(&t, key.public_key().as_ref(), signature.as_ref()));

        t = generate_random_transaction();
        // t = generate_random_transaction();
        assert!(!verify(&t, key.public_key().as_ref(), signature.as_ref()));
    }
}
