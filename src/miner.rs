use crate::transaction::verify;
use crate::crypto::hash::H160;
use crate::network::server::Handle as ServerHandle;
use crate::network::message::Message;

use log::{debug, warn, info};

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;
use std::time::{SystemTime};
use std::sync::{Arc, Mutex};

use crate::transaction::Transaction;
use crate::block::{Block, Header};
use crate::crypto::merkle::MerkleTree;
use crate::crypto::hash::{Hashable};
use crate::blockchain::{Blockchain, Mempool, State};

use std::thread;
use rand::Rng;

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    mempool: Arc<Mutex<Mempool>>,
    state: Arc<Mutex<State>>
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<Mempool>>,
    state: &Arc<Mutex<State>>
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blockchain: blockchain.clone(),
        mempool: mempool.clone(),
        state: state.clone()
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
        debug!("{:?}", SystemTime::now());
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
        debug!("{:?}", SystemTime::now());
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // TODO: actual mining
            let mut rng = rand::thread_rng();

            let mut blockchain = self.blockchain.lock().unwrap();
            let parent = blockchain.tip();

            let difficulty = blockchain.chain[&parent].header.difficulty;
            // Include the same transaction for now.
            let mut temp_content: Vec<Transaction> = Vec::new();
            // let dest : H160 = (hex!("0000000000000000000000000000000000000000")).into();
            // let t = Transaction{nonce: 0, dest:dest, pubkey:[0;32], sign:[0; 32], input:rng.gen::<f32>()};

            let mut mempool = self.mempool.lock().unwrap();

            let mut state = self.state.lock().unwrap();
            //temp_content.push(t);
            // if mempool.queue.len() > 0 {
            //     let t: Transaction = mempool.pop_transaction();
            //     temp_content.push(t);
            // }

            let mut count = 0;

            let mut prev_state = state.block_state[&parent].clone();

            while mempool.queue.len() > 0 && count < 1000 {
                let transaction: Transaction = mempool.pop_transaction();

                let valid = true;

                let temp_pubkey = &transaction.pubkey;
                let mut temp_sign :[u8; 64] = [0; 64];
                temp_sign.copy_from_slice(&[&transaction.sign_p1[..], &transaction.sign_p2[..]].concat());

                if !verify(&transaction, temp_pubkey, &temp_sign) {
                    continue;
                }

                let sender_addr : H160 = ring::digest::digest(&ring::digest::SHA256, &transaction.pubkey).into();

                if !prev_state.contains_key(&sender_addr) {
                    continue;
                }

                if prev_state[&sender_addr].balance < transaction.input || transaction.nonce != prev_state[&sender_addr].nonce + 1 {
                    continue;
                }

                if valid {
                    let receiver_addr = transaction.dest;

                    let mut send_account = prev_state[&sender_addr].clone();
                    send_account.balance -= transaction.input;
                    send_account.nonce += 1;

                    prev_state.insert(sender_addr, send_account);

                    let mut rec_account = prev_state[&receiver_addr].clone();
                    rec_account.balance += transaction.input;

                    prev_state.insert(receiver_addr, rec_account);

                    temp_content.push(transaction);
                    count += 1;
                }

            }


            let merkle_tree = MerkleTree::new(&temp_content);

            // Use SystemTime::now().duration_since(UNIX_EPOCH).expect("").as_millis() to get millisecond.
            let timestamp = SystemTime::now();

            let temp_header = Header{parent: parent, nonce:rng.gen::<u32>(), difficulty: difficulty, timestamp: timestamp, merkle_root:merkle_tree.root()};
            let temp_block = Block{header: temp_header, data: temp_content.clone()};


            if temp_block.hash() <= difficulty {
                blockchain.insert(&temp_block);
                state.block_state.insert(blockchain.tip(), prev_state);
                self.server.broadcast(Message::NewBlockHashes(vec![temp_block.hash()]));
                // info!("Longest Chain {} {}", temp_block.header.parent, temp_block.hash());
                // print block size.
                // let ser = bincode::serialize(&temp_block).unwrap();
                // println!("Block size: {:?}", ser.len());
                log::info!("[State: {:?}]", state.block_state[&blockchain.tip()]);
                log::info!("[{}]-[Tip hash {}] [Parent hash{}]", blockchain.height[&blockchain.tip()], blockchain.tip(), temp_block.header.parent);
            } else {
                mempool.add_back(temp_content);
            }

            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}
