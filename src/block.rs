use serde::{Serialize, Deserialize};
use crate::crypto::hash::{H256, Hashable};
use crate::transaction::Transaction;
use std::time::SystemTime;
use rand::Rng;

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub struct Header {
	pub parent: H256,
	pub nonce: u32,
	pub difficulty: H256,
	pub timestamp: SystemTime,
	pub merkle_root: H256,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
	pub header: Header,
	pub data: Vec<Transaction>
}

impl Hashable for Block {
	fn hash(&self) -> H256 {
		self.header.hash()
	}
}

impl Hashable for Transaction {
	fn hash(&self) -> H256 {
		let ser = bincode::serialize(&self).unwrap();
		ring::digest::digest(&ring::digest::SHA256, &ser).into()
	}
}

impl Hashable for Header {
	fn hash(&self) -> H256 {
		let ser = bincode::serialize(&self).unwrap();
		ring::digest::digest(&ring::digest::SHA256, &ser).into()
	}
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;

    pub fn generate_random_block(parent: &H256) -> Block {
        let mut rng = rand::thread_rng();
        let diff : H256 = (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into();
        let root : H256 = (hex!("0000000000000000000000000000000000000000000000000000000000000000")).into();
        let h = Header{parent: *parent, nonce:rng.gen::<u32>(), difficulty: diff, timestamp: SystemTime::now(), merkle_root:root};
        let d: Vec<Transaction> = Vec::new();
        return Block{header: h, data: d}
    }
}
